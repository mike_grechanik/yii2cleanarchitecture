<?php

use yii\db\Migration;

/**
 * Handles the creation of tables `blogpost` and `blogcomment`.
 */
class m180909_080409_create_blog_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
         
        $this->createTable('{{blogpost}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(256)->notNull(),
            'body' => $this->text()->notNull(),
            'created' => $this->integer()->notNull(),
            'updated' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->createTable('{{blogcomment}}', [
            'id' => $this->primaryKey(),
            'postid' => $this->integer()->notNull(),
            'title' => $this->string(256)->notNull(),
            'body' => $this->text()->notNull(),
            'created' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('blogpost_created_index', '{{blogpost}}', 'created');
        $this->createIndex('blogcomment_postid_index', '{{blogcomment}}', 'postid');
        $this->createIndex('blogcomment_created_index', '{{blogcomment}}', 'created');
        
        $this->addForeignKey('{{blogcomment_fk}}', '{{blogcomment}}', 'postid', '{{blogpost}}', 'id', 'CASCADE', 'RESTRICT');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{blogcomment}}');
        $this->dropTable('{{blogpost}}');
    }
}
