<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@restapi', dirname(dirname(__DIR__)) . '/restapi');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

// use cases
Yii::$container->setSingleton(\common\modules\blog\application\usecase\PostServiceInterface::class, 
    \common\modules\blog\application\usecase\PostService::class);

Yii::$container->setSingleton(\common\modules\blog\application\usecase\CommentServiceInterface::class, 
    \common\modules\blog\application\usecase\CommentService::class);

// repositories
Yii::$container->setSingleton(\common\modules\blog\domain\repository\PostRepositoryInterface::class, 
    \common\modules\blog\infrastructure\repository\PostRepository::class);

Yii::$container->setSingleton(\common\modules\blog\domain\repository\CommentRepositoryInterface::class, 
    \common\modules\blog\infrastructure\repository\CommentRepository::class);

// application
Yii::$container->setSingleton(\common\modules\blog\application\service\TransactionServiceInterface::class, 
 \common\modules\blog\infrastructure\service\TransactionService::class);

Yii::$container->setSingleton(\common\modules\blog\application\service\AppLoggingServiceInterface::class, 
 \common\modules\blog\infrastructure\service\AppLoggingService::class);

Yii::$container->setSingleton(\common\modules\blog\application\service\MethodInjectorServiceInterface::class, 
 \common\modules\blog\infrastructure\service\MethodInjectorService::class);