<?php

namespace common\modules\blog\ui\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use common\modules\blog\ui\forms\CreateCommentForm;
use common\modules\blog\ui\forms\UpdateCommentForm;
use common\modules\blog\application\usecase\CommentServiceInterface;
use common\modules\blog\domain\repository\PostRepositoryInterface;
use common\modules\blog\domain\repository\CommentRepositoryInterface;
use common\modules\blog\application\service\AppLoggingServiceInterface;
use common\modules\blog\ui\forms\search\CommentSearch;

/**
 * Default controller for the `blog` module
 */
class CommentController extends Controller
{
    
    private $service;
    
    private $commentRepository;
    
    private $postRepository;
    
    private $logService;
    
    public function __construct($id, $module, 
            CommentServiceInterface $commentService, 
            CommentRepositoryInterface $commentRepository, 
            PostRepositoryInterface $postRepository, 
            AppLoggingServiceInterface $logService,
            $config = array()) {
        parent::__construct($id, $module, $config);
        $this->service = $commentService;
        $this->commentRepository = $commentRepository;
        $this->postRepository = $postRepository;
        $this->logService = $logService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST'],
                ],
            ],
        ];
    } 
    
   
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }    
    
    public function actionCreate($id)
    {
        if ($model = $this->findPostModel($id)) {
            $model = new CreateCommentForm();

            if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                try {
                    $this->service->addComment($model->title, $model->body, $id);
                    Yii::$app->session->setFlash('New comment has been added successfully');
                    return $this->redirect(['/blog/post/view', 'id' => $id]);
                } catch (\DomainException $e) {
                    $this->logService->log($e->getMessage());
                    throw new ServerErrorHttpException($e->getMessage());
                }

            }            
        }
    }         
    
    public function actionUpdate($id)
    {
        $comment = $this->findModel($id);
        $model = new UpdateCommentForm($comment);
        
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
             try {
                $this->service->updateComment($id, $model->title, $model->body);
                Yii::$app->session->setFlash('The post has been changed');
                return $this->redirect(['view', 'id' => $id]);
            } catch (\DomainException $e) {
                $this->logService->log($e->getMessage());
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
        
        return $this->render('update', ['model' => $model, 'comment' => $comment]);
    }
    
    public function actionDelete($id)
    {
        if ($this->findModel($id)) {
            try {
                $this->service->deleteComment($id);
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                $this->logService->log($e->getMessage());
                throw new ServerErrorHttpException($e->getMessage());
            }        
        }
        
    }      


    protected function findPostModel($id)
    {
        try {
            $model = $this->postRepository->find($id);
            return $model;
        } catch (\Exception $ex) {
            throw new NotFoundHttpException('The requested blog post does not exist.');
        }
    }     
    
    protected function findModel($id)
    {
        try {
            $model = $this->commentRepository->find($id);
            return $model;
        } catch (\Exception $ex) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}
