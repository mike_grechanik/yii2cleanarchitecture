<?php

namespace common\modules\blog\ui\controllers;

use yii\web\Controller;
use common\modules\blog\domain\repository\PostRepositoryInterface;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `blog` module
 */
class SiteController extends Controller
{
    
    private $repository;
    
    public function __construct($id, $module, PostRepositoryInterface $postRepository, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->repository = $postRepository;
    }

  
    
    /**
     * List of posts
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->repository->getMainListQuery(),
        ]);        
        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    

}
