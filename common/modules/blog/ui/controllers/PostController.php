<?php

namespace common\modules\blog\ui\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\modules\blog\ui\forms\CreatePostForm;
use common\modules\blog\ui\forms\UpdatePostForm;
use common\modules\blog\ui\forms\CreateCommentForm;
use common\modules\blog\application\usecase\PostServiceInterface;
use common\modules\blog\domain\repository\PostRepositoryInterface;
use common\modules\blog\domain\repository\CommentRepositoryInterface;
use common\modules\blog\application\service\AppLoggingServiceInterface;
use common\modules\blog\ui\forms\search\PostSearch;

/**
 * Default controller for the `blog` module
 */
class PostController extends Controller
{
    
    private $service;
    
    /**
     * @var PostRepositoryInterface Post repository 
     */
    private $repository;
    
    private $commentRepository;
    
    private $logService;
    
    public function __construct($id, $module, 
            PostServiceInterface $postService, 
            PostRepositoryInterface $postRepository, 
            CommentRepositoryInterface $commentRepository,
            AppLoggingServiceInterface $logService,
            $config = array()) {
        parent::__construct($id, $module, $config);
        $this->service = $postService;
        $this->repository = $postRepository;
        $this->logService = $logService;
        $this->commentRepository = $commentRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }    
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 3;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $commentDataProvider = new ActiveDataProvider([
            'query' => $this->commentRepository->getPostComments($id),
        ]);        
        $commentDataProvider->pagination->pageSize = 3;        
        
        $commentForm = new CreateCommentForm();

        return $this->render('view', [
            'model' => $model, 
            'commentDataProvider' => $commentDataProvider,
            'commentForm' => $commentForm,            
        ]);
    }    
    
    public function actionCreate()
    {
        $model = new CreatePostForm();
        
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            try {
                $id = $this->service->addPost($model->title, $model->body);
                Yii::$app->session->setFlash('New post has been added successfully');
                return $this->redirect(['view', 'id' => $id]);
            } catch (\DomainException $e) {
                $this->logService->log($e->getMessage());
                throw new ServerErrorHttpException($e->getMessage());
            }
            
        }
        
        return $this->render('create', ['model' => $model]);
    }  
    
    public function actionUpdate($id)
    {
        $post = $this->findModel($id);
        $model = new UpdatePostForm($post);
        
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
             try {
                $this->service->updatePost($id, $model->title, $model->body);
                Yii::$app->session->setFlash('The post has been changed');
                return $this->redirect(['view', 'id' => $id]);
            } catch (\DomainException $e) {
                $this->logService->log($e->getMessage());
                throw new ServerErrorHttpException($e->getMessage());
            }
        }
        
        return $this->render('update', ['model' => $model, 'post' => $post]);
    }
    
    public function actionDelete($id)
    {
        if ($this->findModel($id)) {
            try {
                $this->service->deletePost($id);
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                $this->logService->log($e->getMessage());
                throw new ServerErrorHttpException($e->getMessage());
            }        
        }
        
    }     
    
    protected function findModel($id)
    {
        try {
            $model = $this->repository->find($id);
            return $model;
        } catch (\Exception $ex) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}
