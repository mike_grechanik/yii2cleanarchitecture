<?php

namespace common\modules\blog\ui\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\blog\domain\entity\Comment;
use common\modules\blog\domain\repository\CommentRepositoryInterface;

/**
 * Post represents the model behind the search form of `common\modules\blog\domain\entity\Post`.
 */
class CommentSearch extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'postid'], 'integer'],
            [['title', 'body'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $commentRepository = Yii::createObject(CommentRepositoryInterface::class);
        $query = $commentRepository->getQuery();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        
        $query->andFilterWhere([
            'postid' => $this->postid,
        ]);        

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}
