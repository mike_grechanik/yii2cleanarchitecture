<?php

namespace common\modules\blog\ui\forms;


class UpdateCommentForm extends BaseCommentForm
{
    
    private $commentModel;
    
    public function __construct($commentModel, $config = array()) 
    {
        parent::__construct($config);
        $this->commentModel = $commentModel;
        $this->title = $commentModel->title;
        $this->body = $commentModel->body;
    }
    
}
