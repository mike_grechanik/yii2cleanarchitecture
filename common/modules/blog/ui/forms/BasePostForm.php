<?php

namespace common\modules\blog\ui\forms;

use yii\base\Model;

abstract class BasePostForm extends Model
{
    public $title;
    
    public $body;    
    
    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 256],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }    
}
