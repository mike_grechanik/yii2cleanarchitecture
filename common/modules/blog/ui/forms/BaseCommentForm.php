<?php

namespace common\modules\blog\ui\forms;

use yii\base\Model;

abstract class BaseCommentForm extends Model
{
    public $title;
    
    public $body;    
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'postid' => 'Postid',
            'title' => 'Title',
            'body' => 'Body',
            'created' => 'Created',
        ];
    }   
}
