<?php

namespace common\modules\blog\ui\forms;


class UpdatePostForm extends BasePostForm
{
    
    private $postModel;
    
    public function __construct($postModel, $config = array()) 
    {
        parent::__construct($config);
        $this->postModel = $postModel;
        $this->title = $postModel->title;
        $this->body = $postModel->body;
    }
    
}
