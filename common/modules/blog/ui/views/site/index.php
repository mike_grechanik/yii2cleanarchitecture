<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\modules\blog\ui\forms\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Posts';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'created:datetime',
            [
                'header' => 'Posts',
                'content' => function($model, $key, $index, $column) {
                        $title = Html::a(Html::encode($model->title), Url::to(['post/view', 'id' => $model->id]));
                        $title = Html::tag('h3', $title);
                        $body = Html::tag('div', nl2br(Html::encode($model->body)));
                        return $title . $body;
                }                
            ],
        ],
    ]); ?>
</div>
