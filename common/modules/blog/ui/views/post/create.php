<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\blog\ui\forms\CreatePostForm */
/* @var $form ActiveForm */
?>
<div class="create">
 <h1>Creating a new Post</h1>
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'body')->textarea() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Add new post', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- create -->
