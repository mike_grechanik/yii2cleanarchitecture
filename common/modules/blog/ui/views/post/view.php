<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

$this->title = 'View Post:' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';

?>
<div>
    <h1>Post № <?= $model->id ?></h1>
<p>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
    ]) ?>
</p>    
    <div>Title:<div><?= Html::encode($model->title) ?></div></div>
    <div>Body:<div><?= nl2br(Html::encode($model->body)) ?></div></div>
</div>
<h4>Comments:</h4>
<div>
    <?= GridView::widget([
        'dataProvider' => $commentDataProvider,
        'columns' => [
            'created:datetime',
            [
                'header' => 'Comments',
                'content' => function($model, $key, $index, $column) {
                        $title = Html::tag('h3', $model->title);
                        $body = Html::tag('div', nl2br(Html::encode($model->body)));
                        return $title . $body;
                }                
            ],
        ],
    ]); ?>
</div>
<h4>Add comment:</h4>
<div>
    <?php $form = ActiveForm::begin(['action' => ['/blog/comment/create', 'id' => $model->id]]); ?>

        <?= $form->field($commentForm, 'title') ?>
        <?= $form->field($commentForm, 'body')->textarea() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Add comment', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>    
</div>
