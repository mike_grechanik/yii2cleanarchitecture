<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\blog\ui\forms\UpdateCommentForm */
/* @var $form ActiveForm */
$this->title = 'Update Comment:' . $comment->title;
$this->params['breadcrumbs'][] = ['label' => 'Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $comment->title, 'url' => ['view', 'id' => $comment->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="update">
<h1>Updating Comment № <?= $comment->id ?></h1>
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'body')->textarea() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Update comment', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- create -->
