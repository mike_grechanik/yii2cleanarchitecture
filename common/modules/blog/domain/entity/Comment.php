<?php

namespace common\modules\blog\domain\entity;

use common\modules\blog\domain\repository\PostRepositoryInterface;

/**
 * This is the model class for table "blogcomment".
 *
 * @property int $id
 * @property int $postid
 * @property string $title
 * @property string $body
 * @property int $created
 *
 * @property Blogpost $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blogcomment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['postid', 'title', 'body', 'created'], 'required'],
            [['postid', 'created'], 'integer'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 256],
            [['postid'], 'exist', 'skipOnError' => true, 'targetClass' => Blogpost::className(), 'targetAttribute' => ['postid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'postid' => 'Postid',
            'title' => 'Title',
            'body' => 'Body',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Blogpost::className(), ['id' => 'postid']);
    }
    
    /**
     * Factory to create new instances of Comment class
     * 
     * @return \self
     */
    public static function factoryCreateNew($title, $body)
    {
        $comment = new self;
        $comment->assignData($title, $body);
        return $comment;
    }  
    
    public function assignData($title, $body)
    {
        $this->title = $title;
        $this->body = $body;
    }
    
    /**
     * Assign post to this comment
     * 
     * $postRepository is supposed to be injected by DI container
     * 
     * @param integer $postid
     * @param PostRepositoryInterface $postRepository
     */
    public function assignPost($postid, PostRepositoryInterface $postRepository)
    {
        if ($post = $postRepository->find($postid)) {
            $this->postid = $post->id;
        }
    }
}
