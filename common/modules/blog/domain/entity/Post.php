<?php

namespace common\modules\blog\domain\entity;

/**
 * This is the model class for table "blogpost".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $created
 * @property int $updated
 *
 * @property Blogcomment[] $blogcomments
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blogpost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogcomments()
    {
        return $this->hasMany(Blogcomment::className(), ['postid' => 'id']);
    }
    
    /**
     * Factory to create new instances of Post class
     * 
     * @return \self
     */
    public static function factoryCreateNew($title, $body)
    {
        $post = new self;
        $post->assignData($title, $body);
        return $post;
    }

    public function assignData($title, $body)
    {
        $this->title = $title;
        $this->body = $body;
    }
}
