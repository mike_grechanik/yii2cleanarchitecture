<?php
namespace common\modules\blog\domain\repository;

use common\modules\blog\domain\entity\Comment;

interface CommentRepositoryInterface
{
    public function find($id);
    
    public function add(Comment $comment);
    
    public function save(Comment $comment);
    
    public function remove(Comment $comment);
    
    public function getQuery();
    
    public function getPostComments($postid);
}
