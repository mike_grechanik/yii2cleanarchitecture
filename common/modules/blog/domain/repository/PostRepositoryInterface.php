<?php
namespace common\modules\blog\domain\repository;

use common\modules\blog\domain\entity\Post;

interface PostRepositoryInterface
{
    public function find($id);
    
    public function add(Post $post);
    
    public function save(Post $post);
    
    public function remove(Post $post);
    
    public function getQuery();
    
    public function getMainListQuery();
}
