<?php
namespace common\modules\blog\application\usecase;

interface CommentServiceInterface
{
    public function addComment($title, $body, $postId);
    
    public function updateComment($id, $title, $body);
    
    public function deleteComment($id);
}
