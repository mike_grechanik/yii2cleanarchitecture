<?php
namespace common\modules\blog\application\usecase;

interface PostServiceInterface
{
    public function addPost($title, $body);
    
    public function updatePost($id, $title, $body);
    
    public function deletePost($id);
}
