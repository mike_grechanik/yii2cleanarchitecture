<?php
namespace common\modules\blog\application\usecase;

use common\modules\blog\domain\repository\CommentRepositoryInterface;
use common\modules\blog\domain\entity\Comment;
use common\modules\blog\application\service\TransactionServiceInterface;
use common\modules\blog\application\service\MethodInjectorServiceInterface;

class CommentService implements CommentServiceInterface
{
    private $repository;
    
    private $transactionService;
    
    private $methodInjectService;
    
    public function __construct(
            CommentRepositoryInterface $commentRepository, 
            TransactionServiceInterface $transactionService,
            MethodInjectorServiceInterface $methodInjectService) 
    {
        $this->repository = $commentRepository;
        $this->transactionService = $transactionService;
        $this->methodInjectService = $methodInjectService;
    }
    
    public function addComment($title, $body, $postid)
    {
        $transactionService = $this->transactionService;
        $transaction = $transactionService->begin();
        try {
            $comment = Comment::factoryCreateNew($title, $body);
            $this->methodInjectService->invoke([$comment, 'assignPost'], ['postid' => $postid]);
            $this->repository->add($comment);
            $transactionService->commit($transaction);
        } catch(\Exception | \Throwable $e) {
            $transactionService->rollBack($transaction);
            throw new \DomainException('Error at adding comment');
        }        
    }
    
    public function updateComment($id, $title, $body)
    {
        $transactionService = $this->transactionService;
        $transaction = $transactionService->begin();        
        try {
            $comment = $this->repository->find($id);
            $comment->assignData($title, $body);
            $this->repository->save($comment);
            $transactionService->commit($transaction);
        } catch (\Exception | \Throwable $ex) {
            $transactionService->rollBack($transaction);
            throw new \DomainException('Error at updating comment');
        }
    }    
    
    public function deleteComment($id)
    {
        $transactionService = $this->transactionService;
        $transaction = $transactionService->begin();        
        try {
            $comment = $this->repository->find($id);
            $this->repository->remove($comment);
            $transactionService->commit($transaction);
        } catch (\Exception | \Throwable $ex) {
            $transactionService->rollBack($transaction);
            throw new \DomainException('Error at deleting comment');
        }        
    }


}