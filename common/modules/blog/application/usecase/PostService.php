<?php
namespace common\modules\blog\application\usecase;

use common\modules\blog\domain\repository\PostRepositoryInterface;
use common\modules\blog\domain\entity\Post;
use common\modules\blog\application\service\TransactionServiceInterface;

class PostService implements PostServiceInterface
{
    private $repository;
    
    private $transactionService;
    
    public function __construct(PostRepositoryInterface $postRepository, TransactionServiceInterface $transactionService) 
    {
        $this->repository = $postRepository;
        $this->transactionService = $transactionService;
    }
    
    public function addPost($title, $body)
    {
        $transactionService = $this->transactionService;
        $transaction = $transactionService->begin();
        try {
            $post = Post::factoryCreateNew($title, $body);
            $this->repository->add($post);
            $transactionService->commit($transaction);
            return $post->id;
        } catch(\Exception | \Throwable $e) {
            $transactionService->rollBack($transaction);
            throw new \DomainException('Error at adding post');
        }        
    }
    
    public function updatePost($id, $title, $body)
    {
        $transactionService = $this->transactionService;
        $transaction = $transactionService->begin();        
        try {
            $post = $this->repository->find($id);
            $post->assignData($title, $body);
            $this->repository->save($post);
            $transactionService->commit($transaction);
        } catch (\Exception | \Throwable $ex) {
            $transactionService->rollBack($transaction);
            throw new \DomainException('Error at updating post');
        }
    }    
    
    public function deletePost($id)
    {
        $transactionService = $this->transactionService;
        $transaction = $transactionService->begin();        
        try {
            $post = $this->repository->find($id);
            $this->repository->remove($post);
            $transactionService->commit($transaction);
        } catch (\Exception | \Throwable $ex) {
            $transactionService->rollBack($transaction);
            throw new \DomainException('Error at deleting post');
        }        
    }


}