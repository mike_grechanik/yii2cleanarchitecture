<?php
namespace common\modules\blog\application\service;

interface AppLoggingServiceInterface
{
    public function log($message);
}
