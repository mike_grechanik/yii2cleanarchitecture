<?php
namespace common\modules\blog\application\service;

interface TransactionServiceInterface
{
    public function begin();
    public function commit($transaction);
    public function rollback($transaction);
}
