<?php
namespace common\modules\blog\application\service;

interface MethodInjectorServiceInterface
{
    public function invoke(array $method, array $params);
}
