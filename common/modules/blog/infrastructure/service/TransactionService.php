<?php
namespace common\modules\blog\infrastructure\service;

use common\modules\blog\application\service\TransactionServiceInterface;

class TransactionService implements TransactionServiceInterface
{
    public function begin()
    {
        return \Yii::$app->db->beginTransaction();
    }
    public function commit($transaction)
    {
        $transaction->commit();
    }
    public function rollback($transaction)
    {
        $transaction->rollback();
    }
}
