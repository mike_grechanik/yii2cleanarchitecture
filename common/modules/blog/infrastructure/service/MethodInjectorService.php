<?php
namespace common\modules\blog\infrastructure\service;

use common\modules\blog\application\service\MethodInjectorServiceInterface;

class MethodInjectorService implements MethodInjectorServiceInterface
{
    public function invoke(array $method, array $params)
    {
        \Yii::$container->invoke($method, $params);
    }
}
