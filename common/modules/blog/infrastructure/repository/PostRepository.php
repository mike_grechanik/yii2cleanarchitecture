<?php
namespace common\modules\blog\infrastructure\repository;

use common\modules\blog\domain\repository\PostRepositoryInterface;
use common\modules\blog\domain\entity\Post;
use common\modules\blog\domain\entity\Comment;

class PostRepository implements PostRepositoryInterface
{
    /**
     * @param $id
     * @return Post
     * @throws \InvalidArgumentException
     */
    public function find($id)
    {
        if (!$post = Post::findOne($id)) {
            throw new \InvalidArgumentException('Model not found');
        }
        return $post;
    }

    public function add(Post $post)
    {
        if (!$post->getIsNewRecord()) {
            throw new \InvalidArgumentException('Model not exists');
        }
        $post->created = time();
        $post->updated = time();
        $post->insert(false);
    }

    public function save(Post $post)
    {
        if ($post->getIsNewRecord()) {
            throw new \InvalidArgumentException('Model not exists');
        }
        $post->updated = time();
        $post->update(false);
    }    
    
    public function remove(Post $post)
    {
        $post->delete();
        Comment::deleteAll(['postid' => $post->id]);
    }
    
    public function getQuery()
    {
        return Post::find();
    }
    
    public function getMainListQuery()
    {
        return $this->getQuery()->orderBy('created desc');
    }
}