<?php
namespace common\modules\blog\infrastructure\repository;

use common\modules\blog\domain\repository\CommentRepositoryInterface;
use common\modules\blog\domain\entity\Comment;

class CommentRepository implements CommentRepositoryInterface
{
    /**
     * @param $id
     * @return Comment
     * @throws \InvalidArgumentException
     */
    public function find($id)
    {
        if (!$comment = Comment::findOne($id)) {
            throw new \InvalidArgumentException('Model not found');
        }
        return $comment;
    }

    public function add(Comment $comment)
    {
        if (!$comment->getIsNewRecord()) {
            throw new \InvalidArgumentException('Model not exists');
        }
        $comment->created = time();
        $comment->insert(false);
    }

    public function save(Comment $comment)
    {
        if ($comment->getIsNewRecord()) {
            throw new \InvalidArgumentException('Model not exists');
        }
        $comment->update(false);
    }    
    
    public function remove(Comment $comment)
    {
        $comment->delete();
    }
    
    public function getQuery()
    {
        return Comment::find();
    }
    
    public function getPostComments($postid)
    {
        return $this->getQuery()->andWhere(['postid' => $postid])->orderBy('created desc');
    }
}